
This looks for files named `*_slides.org` and `*_article.org` and builds them with ox-beamer.

To run the builder:

    docker run --rm \
	  -v ~/path/to/org/files/:/source/ \
	  -v ~/output/:/output/ \
	  --user 1000 beamer-builder

- [Writing Beamer presentations in org-mode](https://orgmode.org/worg/exporters/beamer/tutorial.html)
- [Beamer export (org-mode manual)](https://orgmode.org/org.html#Beamer-export)
