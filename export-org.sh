#!/bin/bash

EMACS_ARGS=
if [ "x$CONF" != "x" ]; then
    EMACS_ARGS="$EMACS_ARGS -l $CONF"
fi

find $SOURCE -iname '*_slides.org' -o -iname '*_article.org' \
    | xargs -I{} emacs $EMACS_ARGS {} --batch -f org-beamer-export-to-pdf --kill

find $SOURCE -iname '*_slides.tex' \
     -o -iname '*_slides.pdf' \
     -o -iname '*_article.tex' \
     -o -iname '*_article.pdf' \
    | xargs -I{} cp {} $OUTPUT
