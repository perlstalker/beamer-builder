(org-babel-do-load-languages
 'org-babel-load-languages
 '((ditaa . t))) ; this line activates ditaa

(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "ditaa")))  ; don't ask for ditaa
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

(setq org-publish-project-alist
  '(("html"
     :base-directory (getenv "SOURCE")
     :base-extension "org"
     :publishing-directory (getenv "OUTPUT")
     :publishing-function org-publish-org-to-html)
    ("pdf"
     :base-directory (getenv "SOURCE")
     :base-extension "org"
     :publishing-directory (getenv "OUTPUT")
     :publishing-function org-publish-org-to-pdf)
    ("all" :components ("html" "pdf"))))
