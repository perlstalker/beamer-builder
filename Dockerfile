FROM debian:unstable

ENV CONF=/conf/emacs.el \
    SOURCE=/source \
    OUTPUT=/output

RUN apt-get update && \
    apt-get install -y \
      ditaa \
      emacs-nox \
      git \
      texlive-full \
      texlive-latex-recommended \
      texlive-bibtex-extra \
    && apt-get clean

RUN mkdir -p /usr/share/emacs/25.2/lisp/contrib/scripts/ && ln -s /usr/share/ditaa/ditaa.jar /usr/share/emacs/25.2/lisp/contrib/scripts/ditaa.jar

RUN mkdir /source /output /conf

COPY dot-emacs.el /conf/emacs.el
COPY export-org.sh /usr/local/bin/

VOLUME /source
VOLUME /output

USER nobody
CMD  '/usr/local/bin/export-org.sh'

